/*
 * This file is part of GNUnet
 * Copyright (C) 2009-2014 GNUnet e.V.
 *
 * GNUnet is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3, or (at your
 * option) any later version.
 *
 * GNUnet is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNUnet; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
/**
 * @file namestore/plugin_gtk_namestore_srv.c
 * @brief namestore plugin for editing SRV records
 * @author Christian Grothoff
 *
 * Please note that the code of this plugin (and its XML) is
 * included in the BOX plugin (and Box XML) as well and thus
 * particular care needs to be taken when changes are made
 * to make sure names are consistent across the plugins.
 */
#include "gnunet_gtk.h"
#include "gnunet_gtk_namestore_plugin.h"


/**
 * The user has edited the target value.  Enable/disable 'save'
 * button depending on the validity of the value.
 *
 * @param entry editing widget
 * @param user_data the plugin environment
 */
static void
GNS_edit_dialog_srv_target_entry_changed_cb (GtkEditable *entry,
                                             gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  edc->check_validity (edc);
}


#ifndef EDP_CBC_DEF
#define EDP_CBC_DEF
/**
 * The user has changed the protocol selection.  Enable/disable 'save'
 * button depending on the validity of the value.
 *
 * @param entry editing widget
 * @param user_data the plugin environment
 */
static void
edit_dialog_protocol_combobox_changed_cb (GtkEditable *entry,
                                          gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  edc->check_validity (edc);
}
#endif


/**
 * Function that will be called to initialize the builder's
 * widgets from the existing record (if there is one).
 * The `n_value` is the existing value of the record as a string.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param n_value the record as a string
 * @param builder the edit dialog's builder
 */
static void
srv_load (void *cls, gchar *n_value, GtkBuilder *builder)
{
  unsigned int protocol;
  GtkComboBox *cb;
  GtkTreeIter iter;
  unsigned int service;
  guint service_at_iter;
  unsigned int priority;
  unsigned int weight;
  unsigned int port;
  unsigned int record_type;
  char target_name[253 + 1];
  GtkTreeModel *tm;

  if (7 != sscanf (n_value,
                   "%u %u %u %u %u %u %253s",
                   &protocol,
                   &service,
                   &record_type,
                   &priority,
                   &weight,
                   &port,
                   target_name))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                _ ("Unable to parse (boxed) SRV record `%s'\n"),
                n_value);
    return;
  }
  if (GNUNET_DNSPARSER_TYPE_SRV != record_type)
  {
    GNUNET_break (0);
    return;
  }
  gtk_spin_button_set_value (
    GTK_SPIN_BUTTON (
      gtk_builder_get_object (builder, "edit_dialog_port_spinbutton")),
    protocol);
  cb = GTK_COMBO_BOX (
    gtk_builder_get_object (builder, "edit_dialog_protocol_combobox"));
  tm = GTK_TREE_MODEL (
    gtk_builder_get_object (builder, "edit_dialog_protocol_liststore"));
  if (gtk_tree_model_get_iter_first (tm, &iter))
  {
    do
    {
      gtk_tree_model_get (tm, &iter, 1, &service_at_iter, -1);
      if (service_at_iter == service)
      {
        gtk_combo_box_set_active_iter (cb, &iter);
        break;
      }
    } while (gtk_tree_model_iter_next (tm, &iter));
  }
  gtk_spin_button_set_value (
    GTK_SPIN_BUTTON (
      gtk_builder_get_object (builder, "edit_dialog_srv_priority_spinbutton")),
    priority);
  gtk_spin_button_set_value (
    GTK_SPIN_BUTTON (
      gtk_builder_get_object (builder, "edit_dialog_srv_weight_spinbutton")),
    weight);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (gtk_builder_get_object (
                               builder,
                               "edit_dialog_srv_value_port_spinbutton")),
                             port);
  gtk_entry_set_text (GTK_ENTRY (
                        gtk_builder_get_object (builder,
                                                "edit_dialog_srv_target_entry")),
                      target_name);
}


/**
 * Function that will be called to retrieve the final value of the
 * record (in string format) once the dialog is being closed.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return record value as a string, as specified in the dialog
 */
static gchar *
srv_store (void *cls, GtkBuilder *builder)
{
  unsigned int protocol;
  GtkComboBox *cb;
  GtkTreeIter iter;
  guint service;
  unsigned int priority;
  unsigned int weight;
  unsigned int port;
  GtkEntry *entry;
  const gchar *target;
  char *result;
  GtkTreeModel *tm;

  protocol = gtk_spin_button_get_value (GTK_SPIN_BUTTON (
    gtk_builder_get_object (builder, "edit_dialog_port_spinbutton")));
  cb = GTK_COMBO_BOX (
    gtk_builder_get_object (builder, "edit_dialog_protocol_combobox"));
  if (! gtk_combo_box_get_active_iter (cb, &iter))
  {
    GNUNET_break (0);
    return NULL;
  }
  tm = GTK_TREE_MODEL (
    gtk_builder_get_object (builder, "edit_dialog_protocol_liststore"));
  gtk_tree_model_get (tm, &iter, 1, &service, -1);
  priority = gtk_spin_button_get_value (GTK_SPIN_BUTTON (
    gtk_builder_get_object (builder, "edit_dialog_srv_priority_spinbutton")));
  weight = gtk_spin_button_get_value (GTK_SPIN_BUTTON (
    gtk_builder_get_object (builder, "edit_dialog_srv_weight_spinbutton")));
  port = gtk_spin_button_get_value (GTK_SPIN_BUTTON (
    gtk_builder_get_object (builder, "edit_dialog_srv_value_port_spinbutton")));
  entry = GTK_ENTRY (
    gtk_builder_get_object (builder, "edit_dialog_srv_target_entry"));
  target = gtk_entry_get_text (entry);
  GNUNET_asprintf (&result,
                   "%u %u %u %u %u %u %s",
                   protocol,
                   (unsigned int) service,
                   GNUNET_DNSPARSER_TYPE_SRV,
                   priority,
                   weight,
                   port,
                   target);
  return result;
}


/**
 * Function to call to validate the state of the dialog.  Should
 * return #GNUNET_OK if the information in the dialog is valid, and
 * #GNUNET_SYSERR if some fields contain invalid values.  The
 * function should highlight fields with invalid inputs for the
 * user.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return #GNUNET_OK if there is a valid record value in the dialog
 */
static int
srv_validate (void *cls, GtkBuilder *builder)
{
  GtkEditable *entry;
  const gchar *preedit;
  GtkComboBox *cb;
  GtkTreeIter iter;

  entry = GTK_EDITABLE (
    gtk_builder_get_object (builder, "edit_dialog_srv_target_entry"));
  preedit = gtk_editable_get_chars (entry, 0, -1);
  if ((NULL == preedit) || (GNUNET_OK != GNUNET_DNSPARSER_check_name (preedit)))
    return GNUNET_SYSERR;
  cb = GTK_COMBO_BOX (
    gtk_builder_get_object (builder, "edit_dialog_protocol_combobox"));
  if (! gtk_combo_box_get_active_iter (cb, &iter))
    return GNUNET_SYSERR;
  return GNUNET_OK;
}


/**
 * Entry point for the plugin.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment`
 * @return NULL on error, otherwise the plugin context
 */
void *
libgnunet_plugin_gtk_namestore_srv_init (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *env = cls;
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin;
  static struct GNUNET_GTK_NAMESTORE_Symbol symbols[] =
    {{"GNS_edit_dialog_srv_target_entry_changed_cb",
      G_CALLBACK (GNS_edit_dialog_srv_target_entry_changed_cb)},
     /* generic CBs */
     {"edit_dialog_protocol_combobox_changed_cb",
      G_CALLBACK (edit_dialog_protocol_combobox_changed_cb)},
     {NULL, NULL}};

  plugin = GNUNET_new (struct GNUNET_GTK_NAMESTORE_PluginFunctions);
  plugin->cls = env;
  plugin->dialog_glade_filename = "gnunet_namestore_edit_srv.glade";
  plugin->dialog_widget_name = "edit_srv_dialog";
  plugin->symbols = symbols;
  plugin->load = &srv_load;
  plugin->store = &srv_store;
  plugin->validate = &srv_validate;
  /* we will not produce a 'native' SRV record, but one in a BOX */
  plugin->record_type = GNUNET_GNSRECORD_TYPE_BOX;
  return plugin;
}


/**
 * Exit point from the plugin.
 *
 * @param cls the plugin context (as returned by "init")
 * @return always NULL
 */
void *
libgnunet_plugin_gtk_namestore_srv_done (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin = cls;

  GNUNET_free (plugin);
  return NULL;
}

/* end of plugin_gtk_namestore_srv.c */
