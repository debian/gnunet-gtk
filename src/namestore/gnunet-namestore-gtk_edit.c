/*
     This file is part of GNUnet
     Copyright (C) 2012, 2013 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/
/**
 * @file src/namestore/gnunet-namestore-gtk_edit.c
 * @author Christian Grothoff
 * @brief common functions for editing dialogs for GNS records
 */
#include "gnunet_gtk.h"
#include <gnunet/gnunet_gns_service.h>
#include "gnunet_gtk_namestore_plugin.h"


/**
 * The 'relative' expiration time radiobutton was toggled (on or off).
 *
 * @param button the button
 * @param user_data the '' of the dialog
 */
void
GNS_edit_dialog_expiration_relative_radiobutton_toggled_cb (
  GtkToggleButton *button,
  gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  if (gtk_toggle_button_get_active (button))
    gtk_widget_show (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_relative_combobox")));
  else
    gtk_widget_hide (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_relative_combobox")));
}


/**
 * The 'forever' expiration time radiobutton was toggled (on or off).
 *
 * @param button the button
 * @param user_data the '' of the dialog
 */
void
GNS_edit_dialog_expiration_forever_radiobutton_toggled_cb (
  GtkToggleButton *button,
  gpointer user_data)
{
  /* nothing to do */
}


/**
 * The 'absolute' expiration time radiobutton was toggled (on or off).
 *
 * @param button the button
 * @param user_data the '' of the dialog
 */
void
GNS_edit_dialog_expiration_absolute_radiobutton_toggled_cb (
  GtkToggleButton *button,
  gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  if (gtk_toggle_button_get_active (button))
  {
    gtk_widget_show (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_absolute_calendar")));
    gtk_widget_show (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_absolute_hbox")));
  }
  else
  {
    gtk_widget_hide (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_absolute_calendar")));
    gtk_widget_hide (GTK_WIDGET (
      gtk_builder_get_object (edc->builder,
                              "edit_dialog_expiration_absolute_hbox")));
  }
}


/* end of gnunet-namestore-gtk_edit.c */
