/*
     This file is part of GNUnet.
     Copyright (C) 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-options.h
 * @brief configuration details
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_OPTIONS_H
#define GNUNET_SETUP_OPTIONS_H

#include "gnunet-setup.h"


/**
 * Function to setup the value on load.
 *
 * @param cls closure
 * @param section section with the value
 * @param option option name
 * @param value value as a string
 * @param widget widget to initialize
 * @param cfg configuration handle
 * @return GNUNET_OK on success, GNUNET_SYSERR if there was a problem
 */
typedef int (*GNUNET_SETUP_LoadFunction) (
  const void *cls,
  const char *section,
  const char *option,
  const char *value,
  GObject *widget,
  const struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Modify the configuration to contain the right value for
 * the option based on the state of the widget.
 *
 * @param cls closure
 * @param section section with the value
 * @param option option name
 * @param widget widget to initialize
 * @param cfg configuration handle to update
 * @return GNUNET_OK on success, GNUNET_SYSERR if there was a problem
 */
typedef int (*GNUNET_SETUP_SaveFunction) (
  const void *cls,
  const char *section,
  const char *option,
  GObject *widget,
  struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Function called each time the widget changes its state to validate
 * its value.
 *
 * @param cls closure
 * @param widget widget whose state was changed
 */
typedef void (*GNUNET_SETUP_InputValidationFunction) (const void *cls,
                                                      GObject *widget);


/**
 * Structs of this type specify under which conditions the values of
 * a particular option impact the visibility (or sensitivity) of some
 * other widget.
 */
struct GNUNET_SETUP_VisibilitySpecification
{
  /**
   * Which widget should be changed?
   */
  const char *widget_name;

  /**
   * If the option value matchis this regex, the widget should be
   * shown.  If NULL, the "hide_value" controls visibility.
   */
  const char *show_value;

  /**
   * If the option value matchis this regex, the widget should be
   * hidden.  If NULL, the "show_value" controls visibility.
   */
  const char *hide_value;
};


/**
 * Structs of this type define how widgets relate to GNUnet options
 * and control visibility and special actions.
 */
struct GNUNET_SETUP_OptionSpecification
{
  /**
   * Name of the GTK widget in Glade.
   */
  const char *widget_name;

  /**
   * Name of the signal the widget emits if its state changes.
   */
  const char *change_signal;

  /**
   * Section in the configuration
   */
  const char *section;

  /**
   * Name of the configuration option.
   */
  const char *option;

  /**
   * Help text to display for this option.
   */
  const char *help_text;

  /**
   * Help URL to link to for this option.
   */
  const char *help_url;

  /**
   * Function to call to initialize the widget from the configuration.
   */
  GNUNET_SETUP_LoadFunction load_function;

  /**
   * Function to call set the configuration from the widget.
   */
  GNUNET_SETUP_SaveFunction save_function;

  /**
   * Closure for 'validation_function'.
   */
  const void *load_save_cls;

  /**
   * Function to call to validate the value of the widget.
   */
  GNUNET_SETUP_InputValidationFunction input_validation_function;

  /**
   * Closure for 'input_validation_function'.
   */
  const void *input_validation_function_cls;

  /**
   * Visibility changes to apply if this option changes (NULL, or
   * NULL-terminated).
   */
  const struct GNUNET_SETUP_VisibilitySpecification *visibility;
};


/**
 * Option specification data.
 */
extern const struct GNUNET_SETUP_OptionSpecification option_specifications[];

#endif
