/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2013 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup.h
 * @brief globals
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_H
#define GNUNET_SETUP_H

#include "gnunet_gtk.h"
#include <gnunet/gnunet_util_lib.h>
#include <gladeui/glade.h>
#include <gtk/gtk.h>
#include <pwd.h>

/**
 * Name of the configuration file.
 */
extern const char *option_cfg_name;


/**
 * Columns in the gns setup model.
 */
enum GNUNET_GTK_SETUP_HostlistUrlModelColumns
{
  /**
     * A gchararray
     */
  GNUNET_GTK_SETUP_HOSTLIST_URL_MC_URL = 0,

  /**
     * A gboolean
     */
  GNUNET_GTK_SETUP_HOSTLIST_URL_MC_EDITABLE = 1,
};


/**
 * Get an object from the main window.
 *
 * @param name name of the object
 * @return NULL on error, otherwise the object
 */
GObject *
GNUNET_SETUP_get_object (const char *name);

/**
 * Our configuration.
 */
extern struct GNUNET_CONFIGURATION_Handle *cfg;


#endif
/* end of gnunet-setup.h */
