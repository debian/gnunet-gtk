/*
     This file is part of GNUnet.
     Copyright (C) 2013 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-exit-services.h
 * @brief code for the dialog to configure EXIT records
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_EXIT_SERVICES_H
#define GNUNET_SETUP_EXIT_SERVICES_H

#include "gnunet-setup.h"
#include <gnunet/gnunet_util_lib.h>
#include <gdk/gdkkeysyms.h>


/**
 * Initialize the GtkListModel with the hosted service specification.
 *
 * @param cls NULL
 * @param section section with the value (NULL)
 * @param option option name (NULL)
 * @param value value as a string (NULL)
 * @param widget widget to initialize (the GtkTreeView)
 * @param cfg configuration handle
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if there was a problem
 */
int
load_hosted_service_configuration (
  const void *cls,
  const char *section,
  const char *option,
  const char *value,
  GObject *widget,
  const struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Initialize the GtkListModel with the VPN's DNS service specification.
 *
 * @param cls NULL
 * @param section section with the value (NULL)
 * @param option option name (NULL)
 * @param widget widget to initialize (the GtkTreeView)
 * @param cfg configuration handle
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if there was a problem
 */
int
hosted_service_name_install_edited_handler (
  const void *cls,
  const char *section,
  const char *option,
  GObject *widget,
  struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Initialize the GtkListModel with the VPN's DNS service specification.
 *
 * @param cls NULL
 * @param section section with the value (NULL)
 * @param option option name (NULL)
 * @param widget widget to initialize (the GtkTreeView)
 * @param cfg configuration handle
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if there was a problem
 */
int
hosted_service_is_udp_install_toggled_handler (
  const void *cls,
  const char *section,
  const char *option,
  GObject *widget,
  struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Initialize the GtkListModel with the VPN's DNS service specification.
 *
 * @param cls NULL
 * @param section section with the value (NULL)
 * @param option option name (NULL)
 * @param widget widget to initialize (the GtkTreeView)
 * @param cfg configuration handle
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if there was a problem
 */
int
hosted_service_visible_port_install_edited_handler (
  const void *cls,
  const char *section,
  const char *option,
  GObject *widget,
  struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Initialize the GtkListModel with the VPN's DNS service specification.
 *
 * @param cls NULL
 * @param section section with the value (NULL)
 * @param option option name (NULL)
 * @param widget widget to initialize (the GtkTreeView)
 * @param cfg configuration handle
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if there was a problem
 */
int
hosted_service_destination_install_edited_handler (
  const void *cls,
  const char *section,
  const char *option,
  GObject *widget,
  struct GNUNET_CONFIGURATION_Handle *cfg);


#endif
