/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_phone.h
 * @brief main logic to manage phone calls
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_PHONE_H
#define GNUNET_CONVERSATION_GTK_PHONE_H


/**
 * Initialize phone subsystem.
 */
void
GCG_PHONE_init (void);


/**
 * Shutdown phone subsystem.
 */
void
GCG_PHONE_shutdown (void);


#endif
