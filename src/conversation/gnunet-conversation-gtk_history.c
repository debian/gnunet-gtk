/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/
/**
 * @file src/conversation/gnunet-conversation-gtk_history.c
 * @brief manages the call history view
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#include "gnunet-conversation-gtk.h"
#include "gnunet-conversation-gtk_history.h"


/**
 * Columns in the #ego_liststore.
 */
enum HistoryListstoreValues
{
  /**
   * Human-readable time of the event (gchar *)
   */
  HISTORY_LS_TIME = 0,

  /**
   * Human-readable event type (gchar *)
   */
  HISTORY_LS_EVENT = 1,

  /**
   * Human-readable name of the contact involved (gchar *)
   */
  HISTORY_LS_CONTACT = 2,

  /**
   * Unique number associated with the call (guint)
   */
  HISTORY_LS_NUMBER = 3,

  /**
   * Number of the entry used for sorting by timestamp (guint64)
   */
  HISTORY_LS_TIME_ORDER = 4
};


/**
 * Call history liststore
 */
static GtkListStore *history_liststore;


/**
 * Adds entry to the history.
 *
 * @param type type of the event
 * @param contact_name name of the contact person
 * @param call_number unique number for the call
 */
void
GCG_HISTORY_add (enum GCG_HISTORY_Type type,
                 const char *contact_name,
                 guint call_number)
{
  GtkTreeIter iter;
  struct GNUNET_TIME_Absolute now;
  const char *event;
  const char *ts;

  event = NULL; /* just to be safe... */
  switch (type)
  {
  case GCG_HISTORY_TYPE_OUTGOING_CALL:
    event = "Dialing";
    break;
  case GCG_HISTORY_TYPE_OUTGOING_RINGING:
    event = "Waiting";
    break;
  case GCG_HISTORY_TYPE_OUTGOING_ACCEPTED:
    event = "Accepted";
    break;
  case GCG_HISTORY_TYPE_INCOMING_CALL:
    event = "Ringing";
    break;
  case GCG_HISTORY_TYPE_INCOMING_ACCEPTED:
    event = "Answering";
    break;
  case GCG_HISTORY_TYPE_INCOMING_REJECTED:
    event = "Rejected";
    break;
  case GCG_HISTORY_TYPE_INCOMING_MISSED:
    event = "Missed";
    break;
  case GCG_HISTORY_TYPE_SUSPEND_LOCAL:
    event = "Suspending";
    break;
  case GCG_HISTORY_TYPE_RESUMED_LOCAL:
    event = "Resuming";
    break;
  case GCG_HISTORY_TYPE_SUSPEND_REMOTE:
    event = "On hold";
    break;
  case GCG_HISTORY_TYPE_RESUMED_REMOTE:
    event = "Resumed";
    break;
  case GCG_HISTORY_TYPE_HANGUP:
    event = "Hang up";
    break;
  case GCG_HISTORY_TYPE_FAILED:
    event = "Failed";
    break;
  }
  now = GNUNET_TIME_absolute_get ();
  ts = GNUNET_STRINGS_absolute_time_to_string (now);
  gtk_list_store_insert_with_values (history_liststore,
                                     &iter,
                                     -1,
                                     HISTORY_LS_TIME,
                                     ts,
                                     HISTORY_LS_EVENT,
                                     event,
                                     HISTORY_LS_CONTACT,
                                     contact_name,
                                     HISTORY_LS_NUMBER,
                                     call_number,
                                     HISTORY_LS_TIME_ORDER,
                                     (guint64) now.abs_value_us,
                                     -1);
}


/**
 * Intialize history subsystem.
 */
void
GCG_HISTORY_init ()
{
  history_liststore = GTK_LIST_STORE (
    GCG_get_main_window_object ("gnunet_conversation_gtk_history_liststore"));
}

/* end of gnunet-conversation-gtk_history.c */
