/* *INDENT-OFF* */

gint types_generic[] = {EXTRACTOR_METATYPE_MIMETYPE,
                        EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                        EXTRACTOR_METATYPE_TITLE,
                        EXTRACTOR_METATYPE_KEYWORDS,
                        EXTRACTOR_METATYPE_SUBJECT,
                        EXTRACTOR_METATYPE_DESCRIPTION,
                        EXTRACTOR_METATYPE_COMMENT,
#if HAVE_EXTRACTOR_H
                        EXTRACTOR_METATYPE_COPYRIGHT,
                        EXTRACTOR_METATYPE_URI,
                        EXTRACTOR_METATYPE_CREATOR,
                        EXTRACTOR_METATYPE_CREATION_DATE,
#endif
                        EXTRACTOR_METATYPE_RESERVED};


gint types_text[] = {EXTRACTOR_METATYPE_MIMETYPE,
                     EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                     EXTRACTOR_METATYPE_TITLE,
                     EXTRACTOR_METATYPE_KEYWORDS,
#if HAVE_EXTRACTOR_H
                     EXTRACTOR_METATYPE_ABSTRACT,
                     EXTRACTOR_METATYPE_SUMMARY,
                     EXTRACTOR_METATYPE_SUBJECT,
                     EXTRACTOR_METATYPE_AUTHOR_NAME,
                     EXTRACTOR_METATYPE_AUTHOR_EMAIL,
                     EXTRACTOR_METATYPE_AUTHOR_INSTITUTION,
                     EXTRACTOR_METATYPE_DESCRIPTION,
                     EXTRACTOR_METATYPE_LANGUAGE,
                     EXTRACTOR_METATYPE_COPYRIGHT,
                     EXTRACTOR_METATYPE_COMMENT,
                     EXTRACTOR_METATYPE_BOOK_TITLE,
                     EXTRACTOR_METATYPE_BOOK_EDITION,
                     EXTRACTOR_METATYPE_BOOK_CHAPTER_NUMBER,
                     EXTRACTOR_METATYPE_JOURNAL_NAME,
                     EXTRACTOR_METATYPE_JOURNAL_VOLUME,
                     EXTRACTOR_METATYPE_JOURNAL_NUMBER,
                     EXTRACTOR_METATYPE_PAGE_COUNT,
                     EXTRACTOR_METATYPE_PAGE_RANGE,
                     EXTRACTOR_METATYPE_PUBLISHER,
                     EXTRACTOR_METATYPE_PUBLISHER_ADDRESS,
                     EXTRACTOR_METATYPE_PUBLISHER_INSTITUTION,
                     EXTRACTOR_METATYPE_PUBLISHER_SERIES,
                     EXTRACTOR_METATYPE_PUBLICATION_TYPE,
                     EXTRACTOR_METATYPE_PUBLICATION_DATE,
                     EXTRACTOR_METATYPE_URL,
                     EXTRACTOR_METATYPE_CREATOR,
                     EXTRACTOR_METATYPE_CREATION_DATE,
                     EXTRACTOR_METATYPE_MODIFICATION_DATE,
                     EXTRACTOR_METATYPE_CHARACTER_SET,
                     EXTRACTOR_METATYPE_LINE_COUNT,
                     EXTRACTOR_METATYPE_WORD_COUNT,
                     EXTRACTOR_METATYPE_PAGE_ORIENTATION,
                     EXTRACTOR_METATYPE_PAPER_SIZE,
                     EXTRACTOR_METATYPE_TEMPLATE,
                     EXTRACTOR_METATYPE_REVISION_HISTORY,
                     EXTRACTOR_METATYPE_REVISION_NUMBER,
#endif
                     EXTRACTOR_METATYPE_RESERVED};


gint types_music[] = {EXTRACTOR_METATYPE_MIMETYPE,
                      EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                      EXTRACTOR_METATYPE_TITLE,
                      EXTRACTOR_METATYPE_ARTIST,
#if HAVE_EXTRACTOR_H
                      EXTRACTOR_METATYPE_COMPOSER,
                      EXTRACTOR_METATYPE_CONDUCTOR,
                      EXTRACTOR_METATYPE_ALBUM,
                      EXTRACTOR_METATYPE_DURATION,
                      EXTRACTOR_METATYPE_KEYWORDS,
                      EXTRACTOR_METATYPE_DESCRIPTION,
                      EXTRACTOR_METATYPE_GENRE,
                      EXTRACTOR_METATYPE_MOOD,
                      EXTRACTOR_METATYPE_TRACK_NUMBER,
                      EXTRACTOR_METATYPE_LANGUAGE,
                      EXTRACTOR_METATYPE_COMMENT,
                      EXTRACTOR_METATYPE_ISRC,
                      EXTRACTOR_METATYPE_LYRICS,
                      EXTRACTOR_METATYPE_COPYRIGHT,
                      EXTRACTOR_METATYPE_PUBLISHER,
                      EXTRACTOR_METATYPE_PERFORMER,
                      EXTRACTOR_METATYPE_PRODUCER,
                      EXTRACTOR_METATYPE_SONG_VERSION,
                      EXTRACTOR_METATYPE_URI,
                      EXTRACTOR_METATYPE_CREATION_DATE,
                      EXTRACTOR_METATYPE_DISCLAIMER,
                      EXTRACTOR_METATYPE_WRITER,
                      EXTRACTOR_METATYPE_CONTRIBUTOR_NAME,
                      EXTRACTOR_METATYPE_SONG_COUNT,
                      EXTRACTOR_METATYPE_STARTING_SONG,
                      EXTRACTOR_METATYPE_BEATS_PER_MINUTE,
                      EXTRACTOR_METATYPE_ORIGINAL_TITLE,
                      EXTRACTOR_METATYPE_ORIGINAL_ARTIST,
                      EXTRACTOR_METATYPE_ORIGINAL_WRITER,
                      EXTRACTOR_METATYPE_ORIGINAL_RELEASE_YEAR,
                      EXTRACTOR_METATYPE_ORIGINAL_PERFORMER,
                      EXTRACTOR_METATYPE_MUSICIAN_CREDITS_LIST,
                      EXTRACTOR_METATYPE_SUBTITLE,
#endif
                      EXTRACTOR_METATYPE_RESERVED};


gint types_video[] = {EXTRACTOR_METATYPE_MIMETYPE,
                      EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                      EXTRACTOR_METATYPE_TITLE,
#if HAVE_EXTRACTOR_H
                      EXTRACTOR_METATYPE_GENRE,
                      EXTRACTOR_METATYPE_KEYWORDS,
                      EXTRACTOR_METATYPE_SUMMARY,
                      EXTRACTOR_METATYPE_DESCRIPTION,
                      EXTRACTOR_METATYPE_DURATION,
                      EXTRACTOR_METATYPE_IMAGE_DIMENSIONS,
                      EXTRACTOR_METATYPE_COMMENT,
                      EXTRACTOR_METATYPE_CREATOR,
                      EXTRACTOR_METATYPE_WRITER,
                      EXTRACTOR_METATYPE_MOVIE_DIRECTOR,
                      EXTRACTOR_METATYPE_PRODUCER,
                      EXTRACTOR_METATYPE_PUBLISHER,
                      EXTRACTOR_METATYPE_NETWORK_NAME,
                      EXTRACTOR_METATYPE_SHOW_NAME,
                      EXTRACTOR_METATYPE_CHAPTER_NAME,
                      EXTRACTOR_METATYPE_MUSICIAN_CREDITS_LIST,
                      EXTRACTOR_METATYPE_COPYRIGHT,
                      EXTRACTOR_METATYPE_URI,
                      EXTRACTOR_METATYPE_LOCATION_CITY,
                      EXTRACTOR_METATYPE_LOCATION_COUNTRY,
                      EXTRACTOR_METATYPE_LOCATION_COUNTRY_CODE,
                      EXTRACTOR_METATYPE_CREATION_DATE,
                      EXTRACTOR_METATYPE_PERFORMER,
#endif
                      EXTRACTOR_METATYPE_RESERVED};


gint types_image[] = {EXTRACTOR_METATYPE_MIMETYPE,
                      EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                      EXTRACTOR_METATYPE_TITLE,
                      EXTRACTOR_METATYPE_KEYWORDS,
                      EXTRACTOR_METATYPE_DESCRIPTION,
#if HAVE_EXTRACTOR_H
                      EXTRACTOR_METATYPE_IMAGE_DIMENSIONS,
                      EXTRACTOR_METATYPE_IMAGE_RESOLUTION,
                      EXTRACTOR_METATYPE_COMMENT,
                      EXTRACTOR_METATYPE_COPYRIGHT,
                      EXTRACTOR_METATYPE_SUBJECT,
                      EXTRACTOR_METATYPE_CREATOR,
                      EXTRACTOR_METATYPE_CREATION_DATE,
                      EXTRACTOR_METATYPE_URI,
                      EXTRACTOR_METATYPE_LOCATION_CITY,
                      EXTRACTOR_METATYPE_LOCATION_COUNTRY,
                      EXTRACTOR_METATYPE_LOCATION_COUNTRY_CODE,
                      EXTRACTOR_METATYPE_ORGANIZATION,
#endif
                      EXTRACTOR_METATYPE_RESERVED};


gint types_namespace[] = {/* Namespace */
                          EXTRACTOR_METATYPE_MIMETYPE,
                          EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                          EXTRACTOR_METATYPE_COMMENT,
                          EXTRACTOR_METATYPE_TITLE,
                          EXTRACTOR_METATYPE_DESCRIPTION,
#if HAVE_EXTRACTOR_H
                          EXTRACTOR_METATYPE_COPYRIGHT,
                          EXTRACTOR_METATYPE_RIGHTS,
                          EXTRACTOR_METATYPE_KEYWORDS,
                          EXTRACTOR_METATYPE_ABSTRACT,
                          EXTRACTOR_METATYPE_SUMMARY,
                          EXTRACTOR_METATYPE_SUBJECT,
                          EXTRACTOR_METATYPE_CREATOR,
                          EXTRACTOR_METATYPE_RATING,
                          EXTRACTOR_METATYPE_ORGANIZATION,
                          EXTRACTOR_METATYPE_RIPPER,
                          EXTRACTOR_METATYPE_PRODUCER,
                          EXTRACTOR_METATYPE_GROUP,
#endif
                          EXTRACTOR_METATYPE_RESERVED};


gint types_software[] = {EXTRACTOR_METATYPE_MIMETYPE,
                         EXTRACTOR_METATYPE_GNUNET_ORIGINAL_FILENAME,
                         EXTRACTOR_METATYPE_PACKAGE_NAME,
#if HAVE_EXTRACTOR_H
                         EXTRACTOR_METATYPE_PACKAGE_VERSION,
                         EXTRACTOR_METATYPE_TARGET_ARCHITECTURE,
                         EXTRACTOR_METATYPE_LICENSE,
                         EXTRACTOR_METATYPE_TARGET_OS,
                         EXTRACTOR_METATYPE_COMMENT,
                         EXTRACTOR_METATYPE_SECTION,
                         EXTRACTOR_METATYPE_PACKAGE_DEPENDENCY,
                         EXTRACTOR_METATYPE_PACKAGE_CONFLICTS,
                         EXTRACTOR_METATYPE_PACKAGE_REPLACES,
                         EXTRACTOR_METATYPE_PACKAGE_PROVIDES,
                         EXTRACTOR_METATYPE_PACKAGE_RECOMMENDS,
                         EXTRACTOR_METATYPE_PACKAGE_SUGGESTS,
                         EXTRACTOR_METATYPE_PACKAGE_MAINTAINER,
                         EXTRACTOR_METATYPE_PACKAGE_INSTALLED_SIZE,
                         EXTRACTOR_METATYPE_PACKAGE_SOURCE,
                         EXTRACTOR_METATYPE_PACKAGE_ESSENTIAL,
                         EXTRACTOR_METATYPE_PACKAGE_PRE_DEPENDENCY,
                         EXTRACTOR_METATYPE_PACKAGE_DISTRIBUTION,
                         EXTRACTOR_METATYPE_BUILDHOST,
                         EXTRACTOR_METATYPE_VENDOR,
                         EXTRACTOR_METATYPE_SOFTWARE_VERSION,
                         EXTRACTOR_METATYPE_TARGET_PLATFORM,
                         EXTRACTOR_METATYPE_RESOURCE_TYPE,
                         EXTRACTOR_METATYPE_LIBRARY_SEARCH_PATH,
                         EXTRACTOR_METATYPE_LIBRARY_DEPENDENCY,
                         EXTRACTOR_METATYPE_UPLOAD_PRIORITY,
#endif
                         EXTRACTOR_METATYPE_RESERVED};


/**
 * Must match data in 'gnunet_fs_gt_main_window.glade'.
 * FIXME-FEATURE-UNCLEAN-MAYBE: should probably move that name list in
 * here and only have one place for both...
 */
gint *types[] = {types_generic,
                 types_text,
                 types_music,
                 types_video,
                 types_image,
                 types_software,
                 types_namespace,
                 NULL};

/* *INDENT-ON* */
