/*
     This file is part of GNUnet
     Copyright (C) 2012 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_unindex.h
 * @author Christian Grothoff
 */
#ifndef GNUNET_FS_GTK_UNINDEX_H
#define GNUNET_FS_GTK_UNINDEX_H

#include "gnunet-fs-gtk.h"
#include "gnunet-fs-gtk_common.h"
#include "gnunet-fs-gtk_event-handler.h"


/**
 * Information for a specific unindex operation.
 */
struct UnindexEntry;


/**
 * An unindex operation resumed.  Setup the
 * internal context for Gtk.
 *
 * @param uc unindex context with the FS library
 * @param filename name of file being unindexed
 * @param filesize size of the file
 * @param completed how many bytes were done so far
 * @param emsg NULL if everything is fine, otherwise error message
 * @return entry for the resumed operation
 */
struct UnindexEntry *
GNUNET_FS_GTK_unindex_handle_resume_ (struct GNUNET_FS_UnindexContext *uc,
                                      const char *filename,
                                      uint64_t filesize,
                                      uint64_t completed,
                                      const char *emsg);


/**
 * FS notified us that our unindex operation was stopped.
 *
 * @param ue operation that stopped
 */
void
GNUNET_FS_GTK_unindex_handle_stop_ (struct UnindexEntry *ue);


/**
 * FS notified us that our unindex operation had an error.
 *
 * @param ue operation that
 * @param emsg error message
 */
void
GNUNET_FS_GTK_unindex_handle_error_ (struct UnindexEntry *ue, const char *emsg);


/**
 * FS notified us that our unindex operation made progress
 *
 * @param ue operation that made progress
 * @param completed number of bytes completed now
 */
void
GNUNET_FS_GTK_unindex_handle_progress_ (struct UnindexEntry *ue,
                                        uint64_t completed);


/**
 * FS notified us that our unindex operation completed
 *
 * @param ue operation that completed
 */
void
GNUNET_FS_GTK_unindex_handle_completed_ (struct UnindexEntry *ue);


#endif
